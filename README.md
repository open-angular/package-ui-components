# Supported Technology
- Components must be developed using angular directives.

# Conventions
- It is crucial that while developing a component, a strict folder structure and naming guideline must be followed.

## Images structure
```
 Images (D)
   |_component_1 (D)
   . |_logo.ico (F)
   . |_brand.svg (F)
   . |_image.jpg (F)
   .
   |_component_2 (D)
     |_logo.ico (F)
     |_brand.svg(F)
     |_image.jpg
```

## Fonts structure
```
 Fonts (D)
   |_component_1 (D)
   . |_logo-font.tff (F)
   .
   |_component_2 (D)
     |_header-font.tff (F)
```

## Component's scripts structure
```
 Components (D)
   |_component_1 (D)
   . |_component_1.js (F)       * Main file (entrypoint file) must be the same the the component name
   .
   |_component_2 (D)
     |_component_1.js (F)
     |_helper-utility.js (F)
```

## Styles structure
``` 
 Styles (D)
   |_component_1.scss (F)
   |_component_2.scss (F)
```

## Template structure
```
 Templates (D)
   |_component_1 (D)
   . |_template_1.html (F)
   . |_template_2.html (F)
   .
   |_component_2 (D)
     |_template_1.html (F)
   
```

- Each component will belong to its own module
- Module name has syntax of '``<module-prefix>.<component-name-in-camel-case>``'

```
 angular.module('my.components.inventoryTemplate', []).directives('inventoryTemplate', function(){});
```

# Getting started
- Add the following line to **package.json**
```json
  "devDependencies": {
     ...
     "package-components": "https://gitlab.com/open-angular/package-ui-components.git"
  }
```

- Inside **Gruntfile.js**, acquire the task
```javascript
 module.exports = function (grunt) {
    require('package-components')(grunt);
    ...
 }
```

- Under **grunt.initConfig**, add the following configuration
```javascript
    packageComponents: {
        options: {
            modulePrefixes: ["<list of supported prefix>"],
            componentsDir: "<root folder contains all the component's scripts>",
            stylesDir: "<root folder contains all the component's styles>",
            fontsDir: "<root folder contains all the component's fonts>",
            imagesDir: "<root folder contains all the component's images>",
            templatesDir: "<root folder contains all the component's html>",
            templatesCwd: "<current working directory of component's html>",
            grandComponentName: "<name of the script to contains all components (in hyphen-naming-convention)>",
            version: "<packaged component's version"
        },
        dist:{
            dest: "<location folder where all the components will be packaged into>"
        }
    }

```