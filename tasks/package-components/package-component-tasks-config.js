var utility = require('../component-utility');

function ConfigHolder() {};


ConfigHolder.prototype.getCleanComponentDistTaskTarget = function(distDir) {
    return {
        files: [
            {
                dot: true,
                src: [
                    '.tmp',
                    distDir
                ]
            }
        ]
    };
};

ConfigHolder.prototype.getConcatTaskTarget = function(componentsDir) {
    return {
        src: [
            componentsDir + '/<%= grunt.task.current.args[0] %>/{,*/}*.js',
            '.tmp/<%= grunt.task.current.args[0] %>/templates/*.js'
        ],
        dest: '.tmp/<%= grunt.task.current.args[0] %>/scripts/<%= grunt.task.current.args[0] %>.js'
    };
};


/**
 * Concat all concatenated scripts from each component to components.js file
 * @returns {{src: string[], dest: string}}
 */
ConfigHolder.prototype.getConcatEachComponentScriptsToGrandComponentTaskTarget = function(grandComponentName) {
    return {
        src: [
            utility.constructPath('.tmp', grandComponentName, 'scripts', grandComponentName + '.js'),
            '.tmp/<%= grunt.task.current.args[0] %>/scripts/<%= grunt.task.current.args[0] %>.js'
        ],
        dest: utility.constructPath('.tmp', grandComponentName, 'scripts', grandComponentName + '.js')
    };
};



/**
 * Concat all the ng-templates from given components to components.tpls.js file
 *
 * */
ConfigHolder.prototype.getConcatTplsToGrandComponentTaskTarget = function(grandComponentName) {
    return {
        src: [
            utility.constructPath('.tmp', grandComponentName, 'templates', grandComponentName + '.tpls.js'),
            '.tmp/<%= grunt.task.current.args[0] %>/templates/<%= grunt.task.current.args[0] %>.tpls.js'
        ],
        dest: utility.constructPath('.tmp', grandComponentName, 'templates', grandComponentName + '.tpls.js')
    };
};

ConfigHolder.prototype.getCopyComponentFontsTaskTarget = function(fontsDir) {
    return {
        files: [{
            expand: true,
            cwd: fontsDir,
            dest: '.tmp/<%= grunt.task.current.args[0] %>/',
            src: [
                '*.*',
                '<%= grunt.task.current.args[0] %>/{,*/}*.*'
            ]
        }]
    };
};


/**
 * Copy component's assets (images, fonts) and scripts to dist folder,
 *
 * */
ConfigHolder.prototype.getCopyComponentAssetsAndScriptsToDistTaskTarget = function(distDir) {
    return {
        files: [{
            expand: true,
            dot: true,
            cwd: '.tmp/<%= grunt.task.current.args[0] %>',
            dest: distDir + '/<%= grunt.task.current.args[0] %>',
            src: [
                'images/{,*/}*.*',
                'fonts/{,*/}*.*'
            ]
        },{
            expand: true,
            cwd: '.tmp/<%= grunt.task.current.args[0] %>/scripts',
            src: '<%= grunt.task.current.args[0] %>.js',
            dest: distDir + '/<%= grunt.task.current.args[0] %>'
        },{
            expand: true,
            cwd: '.tmp/<%= grunt.task.current.args[0] %>/templates',
            src: '*.js',
            dest: distDir + '/<%= grunt.task.current.args[0] %>'
        }]
    };
};

ConfigHolder.prototype.getCopyComponentAssetsToGrandComponent = function(grandComponentName) {
    return {
        files: [
            {
                //component's image,svg files
                expand: true,
                cwd: '.tmp/<%= grunt.task.current.args[0] %>/images',
                src: ['{,*/}*.*'],
                dest: utility.constructPath('.tmp', grandComponentName, 'images')
            }, {
                //component's fonts files
                expand: true,
                cwd: '.tmp/<%= grunt.task.current.args[0] %>/fonts',
                src: ['{,*/}*.*'],
                dest: utility.constructPath('.tmp', grandComponentName, 'fonts')
            }
        ]
    };
};

ConfigHolder.prototype.getCopyGrandComponentAssetsAndScriptsToDist = function(distDir, grandComponentName) {
    return {
        files: [
            {
                //image,svg files
                expand: true,
                cwd: utility.constructPath('.tmp', grandComponentName, 'images'),
                src: ['{,*/}*.*'],
                dest: utility.constructPath(distDir, grandComponentName, 'images')
            }, {
                //component's fonts files
                expand: true,
                cwd: utility.constructPath('.tmp', grandComponentName, 'fonts'),
                src: ['{,*/}*.*'],
                dest: utility.constructPath(distDir, grandComponentName, 'fonts')
            }, {
                expand: true,
                cwd: utility.constructPath('.tmp', grandComponentName, 'scripts'),
                src: '*.js',
                dest: utility.constructPath(distDir, grandComponentName)
            },{
                expand: true,
                cwd: utility.constructPath('.tmp', grandComponentName, 'templates'),
                src: '*.js',
                dest: utility.constructPath(distDir, grandComponentName)
            }
        ]
    };
};


ConfigHolder.prototype.getNgTemplatesTaskTarget = function(templatesDir, cwd) {
    var config= {
        options: {
            module: '<%= grunt.task.current.args[1] %>',
            htmlmin: {
                collapseWhitespace: true,
                conservativeCollapse: true,
                collapseBooleanAttributes: true,
                removeCommentsFromCDATA: true
            },
        },
        src: [templatesDir + '/<%= grunt.task.current.args[0] %>/{,*/}*.html'],
        dest: '.tmp/<%= grunt.task.current.args[0] %>/templates/<%= grunt.task.current.args[0] %>.tpls.js'
    };

    if (cwd) {
        config['cwd'] = cwd;
    }

    return config;
};

ConfigHolder.prototype.getConcurrentTaskTarget = function() {
    return [
        'compass:<%= grunt.task.current.target %>:<%= grunt.task.current.args[0] %>',
        'imagemin:<%= grunt.task.current.target %>:<%= grunt.task.current.args[0] %>',
        'svgmin:<%= grunt.task.current.target %>:<%= grunt.task.current.args[0] %>'
    ];
};

ConfigHolder.prototype.getCompassTaskTarget = function(stylesDir) {
    return {
        options: {
            //sassDir: '.tmp/<%= grunt.task.current.args[0] %>/scss/<%= grunt.task.current.args[0] %>',
            sassDir: stylesDir,
            specify: stylesDir + '/<%= grunt.task.current.args[0] %>.scss',
            cssDir: '.tmp/<%= grunt.task.current.args[0] %>/styles'
        }
    };
};

ConfigHolder.prototype.getCompassForGrandComponentTaskTarget = function(stylesDir, grandComponentName) {
    return {
        options: {
            //sassDir: '.tmp/<%= grunt.task.current.args[0] %>/scss/<%= grunt.task.current.args[0] %>',
            sassDir: stylesDir,
            cssDir: utility.constructPath('.tmp', grandComponentName, 'styles')
        }
    };
};

ConfigHolder.prototype.getImageminTaskTarget = function(imageDir) {
    return {
        files: [{
            expand: true,
            cwd: imageDir,
            src: '*.{png,jpg,jpeg,gif}',
            dest: '.tmp/<%= grunt.task.current.args[0] %>/images'
        }, {
            expand: true,
            cwd: imageDir + '/<%= grunt.task.current.args[0] %>',
            src: '{,*/}*.{png,jpg,jpeg,gif}',
            dest: '.tmp/<%= grunt.task.current.args[0] %>/images/<%= grunt.task.current.args[0] %>'
        }]
    };
};


ConfigHolder.prototype.getSvgminTaskTarget = function(imageDir) {
    return {
        files: [{
            expand: true,
            cwd: imageDir,
            src: '*.svg',
            //src: [],    //to be filled by build task
            dest: '.tmp/<%= grunt.task.current.args[0] %>/images'
        }, {
            expand: true,
            cwd: imageDir + '/<%= grunt.task.current.args[0] %>',
            src: '{,*/}*.svg',
            //src: [],    //to be filled by build task
            dest: '.tmp/<%= grunt.task.current.args[0] %>/images/<%= grunt.task.current.args[0] %>'
        }]
    };
};

ConfigHolder.prototype.getNgAnnotateTaskTarget = function() {
    return {
        files: [{
            expand: true,
            cwd: '.tmp/<%= grunt.task.current.args[0] %>/scripts',
            src: '*.js',
            dest: '.tmp/<%= grunt.task.current.args[0] %>/scripts'
        }]
    };
};

ConfigHolder.prototype.getNgAnnotateGrandComponentTaskTarget = function(grandComponentName) {
    return {
        files: [{
            expand: true,
            cwd: utility.constructPath('.tmp', grandComponentName, 'scripts'),
            src: '*.js',
            dest: utility.constructPath('.tmp', grandComponentName, 'scripts')
        }]
    };
};


ConfigHolder.prototype.getCssminTaskTarget = function(distDir) {
    var destFileName = distDir + '/<%= grunt.task.current.args[0] %>/styles/main.css';
    var obj = {};
    obj[destFileName] = ['.tmp/<%= grunt.task.current.args[0] %>/styles/{,*/}*.css'];

    return {
        files: [obj]
    };
};


/**
 * minimize all css for all components
 *
 * */
ConfigHolder.prototype.getCssminToGrandComponentDistTaskTarget = function(distDir, grandComponentName) {
    var destFileName = utility.constructPath(distDir, grandComponentName, 'styles', 'main.css');
    var obj = {};
    obj[destFileName] = [utility.constructPath('.tmp', grandComponentName, 'styles', '{,*/}*.css')];

    return {
        files: [obj]
    };
};

ConfigHolder.prototype.getUglifyTaskTarget = function(distDir) {
    return {
        src: ['.tmp/<%= grunt.task.current.args[0] %>/scripts/<%= grunt.task.current.args[0] %>.js'],
        dest: distDir + '/<%= grunt.task.current.args[0] %>/<%= grunt.task.current.args[0] %>.min.js'
    };
};

ConfigHolder.prototype.getUglifyGrandComponentTaskTarget = function(distDir, grandComponentName) {
    return {
        src: [utility.constructPath('.tmp', grandComponentName, 'scripts', grandComponentName + '.js')],
        dest: utility.constructPath(distDir, grandComponentName, grandComponentName + '.min.js')
    };
};

module.exports = new ConfigHolder();
