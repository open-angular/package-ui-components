
function ComponentStructureInspector(grunt) {
    var crawler = require('../angular-file-crawler');
    var utility = require('../component-utility');
    var _ = require('lodash');

    var foundComponents = {
        componentMap: {},
        moduleMap: {},
        orderedComponents: [],
    };

    /**
     *  Turn string of item separated by comma into array.
     * @param phrase
     */
    function string2Array(phrase) {
        var split = phrase.split(/\s*,\s*/);
        return split;
    }

    function parseForSupportedDependencyModules(componentDirectory, dependentModules, supportedModulePrefix) {
        if (!dependentModules) {
            return;
        }

        var supportedModules = crawler.filterForSupportedModules(dependentModules, supportedModulePrefix);
        var componentNames = utility.getDirectiveNamesFromModules(supportedModules).map(utility.toHyphenFormat);

        grunt.log.writeln('Component Names: ' + componentNames);
        componentNames.forEach(function(componentName) {
            parseComponent(componentDirectory, componentName);
        });
    }


    function parseComponent(componentDirectory, componentName, supportedModulePrefixes) {
        if (isComponentRegistered(componentName)) {
            return;
        }

        var mainJsFilePath = utility.constructPath(componentDirectory, componentName, componentName + ".js");
        var moduleInfo = parseContentForModuleInfo(mainJsFilePath);

        var component = {
            moduleName: moduleInfo.moduleName,
            directiveCamelName: moduleInfo.directiveCamelName,
            directiveHyphenName: moduleInfo.directiveHyphenName,
            dependentModules: moduleInfo.dependentModules,
            componentName: componentName
        };

        parseForSupportedDependencyModules(componentDirectory, moduleInfo.dependentModules, supportedModulePrefixes)
        registerComponents([component]);
    }

    function isComponentRegistered(componentName) {
        var component = foundComponents.componentMap[componentName];

        return component;
    }

    function parseComponentFromPath(directiveDirectoryPath) {
        var directoryName = utility.getFileOrDirectoryName(directiveDirectoryPath);
        var mainJsFilePath = utility.constructPath(directiveDirectoryPath, directoryName + ".js");
        var moduleInfo = parseContentForModuleInfo(mainJsFilePath);

        var component = {
            moduleName: moduleInfo.moduleName,
            directiveCamelName: moduleInfo.directiveCamelName,
            directiveHyphenName: moduleInfo.directiveHyphenName,
            dependentModules: moduleInfo.dependentModules,
            componentName: directoryName
        };

        return component;
    }

    function parseContentForModuleInfo(filePath) {
        var flattenContent = crawler.flattenContent(grunt.file.read(filePath));

        var moduleName = crawler.getModuleName(flattenContent);
        var directiveName = crawler.getDirectiveName(flattenContent);
        var dependencies = crawler.getDependentModules(flattenContent);

        return {
            moduleName: moduleName,
            directiveCamelName: utility.toLowerCamelFormat(directiveName),
            directiveHyphenName: utility.toHyphenFormat(directiveName),
            dependencies: dependencies
        };
    }

    function getComponentsFromDirectory(componentDirectory, supportedModulePrefixes) {
        if (!supportedModulePrefixes) {
            supportedModulePrefixes = [];
        }

        var paths = grunt.file.expand({
            filter: 'isDirectory', cwd: '.'
        }, componentDirectory + "/*");

        paths.forEach(function(path) {
            var componentName = utility.getFileOrDirectoryName(path);
            parseComponent(componentDirectory, componentName, supportedModulePrefixes);
        });

        return foundComponents.orderedComponents;
    }


    function registerComponents(components) {
        if (!_.isArray(components)){
            components = [components];
        }

        components.forEach(function(component) {
            var componentName = component.componentName;
            var moduleName = component.moduleName;

            if (foundComponents.componentMap[componentName]) {
                return;
            }

            foundComponents.componentMap[componentName] = component;
            foundComponents.moduleMap[moduleName] = component;

            foundComponents.orderedComponents.push(component);
        });
    }

    function getComponentFromPath(componentPath) {
        var directoryName = utility.getFileOrDirectoryName(componentPath);
        if (directoryName in foundComponents.componentMap) {
            return;
        }

        //start logic
        var component = parseComponentFromPath(componentPath);
        return component;
    }

    return {
        getComponentInfoList: function(componentPaths){
            var components = componentPaths.map(getComponentFromPath);
            return components;
        },

        getComponentMatrixList: function(componentsDir, supportedModulePrefixes) {
            return getComponentsFromDirectory(componentsDir, supportedModulePrefixes);
        }
    };
}


module.exports = function(grunt) {
    return new ComponentStructureInspector(grunt);
};
