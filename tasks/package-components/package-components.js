module.exports = function (grunt) {

    var _ = require('lodash');
    var fs = require('fs');
    var configHolder = require('./package-component-tasks-config.js');
    var inspector = require('./components-structure-inspector.js')(grunt);
    var utility = require('../component-utility');

    function info(msg) {
        grunt.log.writeln(msg);
    }

    function updateConfig(taskName, getNewConfigFn) {
        var config = grunt.config(taskName);

        var updatedConfig = getNewConfigFn;
        if (_.isFunction(getNewConfigFn)) {
            updatedConfig = getNewConfigFn(config);
        }

        grunt.config(taskName, updatedConfig);
    }

    var packageTargetName = 'package';

    function injectTargetToTask(taskName, targetName, config) {
        updateConfig(taskName, function (oldConf) {
            if (!oldConf){
                oldConf = {};
            }

            oldConf[targetName] = config;
            return oldConf;
        });
    }

    /**
     * targets: {
         *      'copy_dist' : {config}
         *      'copy_sass' : {config}
         * }
     * @param taskName
     * @param targets
     */
    function injectTargetsToTask(taskName, targetsOrUpdateTargetBuilderFn) {
        var targets = targetsOrUpdateTargetBuilderFn;
        if (_.isFunction(targetsOrUpdateTargetBuilderFn)) {
            targets = {};
            targetsOrUpdateTargetBuilderFn(targets);
        }

        updateConfig(taskName, function (oldConf) {
            _.forEach(targets, function (config, targetName) {
                if (!oldConf){
                    oldConf = {};
                }

                oldConf[targetName] = config;
            });
            return oldConf;
        });
    }


    function joinSegments(firstSegment, secondSegment, joinPhrase) {
        if (!firstSegment) {
            return secondSegment;
        }

        if (!secondSegment) {
            return firstSegment;
        }

        if (firstSegment.endsWith(joinPhrase)) {
            return firstSegment + secondSegment;
        }

        return firstSegment + joinPhrase + secondSegment;
    }

    function constructGruntTarget() {
        var target = null;

        var targetSegments = _.values(arguments);
        targetSegments.forEach(function (segment) {
            target = joinSegments(target, segment, ":");
        });

        return target;
    }

    function printConfig(configName, target) {
        var config = grunt.config(configName);
        var configTarget = config[target];
        info('Config of ' + configName + ':' + target + ' : ');

        printObject(configTarget);
    }

    function printObject(obj) {
        _.forEach(obj, function (value, key) {
            if (_.isObject(value)) {
                printObject(value);
            } else {
                info(key + ': ' + value);
            }
        });

        info('--end--');
    }

    /**
     * Register all the required task's targets and their configuration
     * @param options
     */
    function registerTaskConfiguration(options, grandComponentName, componentDistDir) {
        injectTargetsToTask('concat', function (map) {
            map[packageTargetName] = configHolder.getConcatTaskTarget(options.componentsDir);
            map[concatTplsToGrantComponentTargetName] = configHolder.getConcatTplsToGrandComponentTaskTarget(grandComponentName);
            map[concatScriptsToGrantComponentTargetName] = configHolder.getConcatEachComponentScriptsToGrandComponentTaskTarget(grandComponentName);
        });

        injectTargetToTask('ngtemplates', packageTargetName, configHolder.getNgTemplatesTaskTarget(options.templatesDir, options.templatesCwd));

        injectTargetsToTask('compass', function (map) {
            map[packageTargetName] = configHolder.getCompassTaskTarget(options.stylesDir);
            map[compassToGrandComponentTargetName] = configHolder.getCompassForGrandComponentTaskTarget(options.stylesDir, grandComponentName);
        });

        injectTargetToTask('imagemin', packageTargetName, configHolder.getImageminTaskTarget(options.imagesDir));
        injectTargetToTask('svgmin', packageTargetName, configHolder.getSvgminTaskTarget(options.imagesDir));
        injectTargetToTask('concurrent', packageTargetName, configHolder.getConcurrentTaskTarget());


        injectTargetsToTask('ngAnnotate', function (map) {
            map[packageTargetName] = configHolder.getNgAnnotateTaskTarget();
            map[annotateGrandComponentTaskTargetName] = configHolder.getNgAnnotateGrandComponentTaskTarget(grandComponentName);
        });


        injectTargetsToTask('copy', function (map) {
            map[copyComponentToDiskTargetName] = configHolder.getCopyComponentAssetsAndScriptsToDistTaskTarget(componentDistDir);
            map[copyComponentFontsTargetName] = configHolder.getCopyComponentFontsTaskTarget(options.fontsDir);
            map[copyEachComponentToGrandComponentsTaskName] = configHolder.getCopyComponentAssetsToGrandComponent(grandComponentName);
            map[copyGrandComponentsAssetsAndScriptsToDistTargetName] = configHolder.getCopyGrandComponentAssetsAndScriptsToDist(componentDistDir, grandComponentName);
        });


        injectTargetsToTask('cssmin', function (map) {
            map[packageTargetName] = configHolder.getCssminTaskTarget(componentDistDir);
            map[cssminToGrandComponentDistTargetName] = configHolder.getCssminToGrandComponentDistTaskTarget(componentDistDir, grandComponentName);
        });

        injectTargetsToTask('uglify', function (map) {
            map[uglifyGrandComponentTaskTargetName] = configHolder.getUglifyGrandComponentTaskTarget(componentDistDir, grandComponentName);
            map[packageTargetName] = configHolder.getUglifyTaskTarget(componentDistDir);
        });

        injectTargetToTask('clean', cleanComponentDistTaskName, configHolder.getCleanComponentDistTaskTarget(componentDistDir))

    }

    //target names
    var concatTplsToGrantComponentTargetName = packageTargetName + 'tpls_to_grand_components';
    var concatScriptsToGrantComponentTargetName = packageTargetName + '_scripts_to_grand_components';
    var compassToGrandComponentTargetName = packageTargetName + '_compass_to_grand_components';
    var annotateGrandComponentTaskTargetName = packageTargetName + '_annotate_grand_components';

    var copyEachComponentToGrandComponentsTaskName = packageTargetName + '_to_grant_components';
    var copyComponentToDiskTargetName = packageTargetName + '_dist';
    var copyComponentFontsTargetName = packageTargetName + '_fonts';
    var copyGrandComponentsAssetsAndScriptsToDistTargetName = packageTargetName + '_grand_components_assets_scripts_to_dist';

    var cssminToGrandComponentDistTargetName = packageTargetName + '_cssmin_to_grand_components_dist';
    var uglifyGrandComponentTaskTargetName = packageTargetName + '_uglify_grand_components';
    var cleanComponentDistTaskName = packageTargetName + '_component_dist';

    /**
     * Return a list of tasks targeted against given component.
     * @param component
     * @param options
     * @returns {Array}
     */
    function getComponentGruntTasks(component, options) {
        var componentName = component.componentName;

        var styleFilePath = utility.constructPath(options.stylesDir, componentName + '.scss');
        var tasks = [];

        //ngtemplates turn any html file into javascript content
        tasks.push(constructGruntTarget('ngtemplates', packageTargetName, componentName, component.moduleName));

        //make css files out of scss and copy them to tmp folder
        if (grunt.file.exists(styleFilePath)) {
            tasks.push(constructGruntTarget('compass', packageTargetName, componentName));
        }

        //minimize images, svg and copy to tmp folder
        tasks.push(constructGruntTarget('imagemin', packageTargetName, componentName));
        tasks.push(constructGruntTarget('svgmin', packageTargetName, componentName));

        //copy fonts to tmp folder
        tasks.push(constructGruntTarget('copy', copyComponentFontsTargetName, componentName));

        //copy component's assets (images, svg and fonts) to grand components folder in tmp
        tasks.push(constructGruntTarget('copy', copyEachComponentToGrandComponentsTaskName, componentName));

        //combine all the javascript files
        tasks.push(constructGruntTarget('concat', packageTargetName, componentName));

        //combine the concatenated javascript from given component to grand components file.
        //It is essential to do this step here since the components are register following the
        //their dependency hierarchy. This way, all the leaf components will be concatenated into grand components
        // script file first
        //1. Concat the full scripts with template
        tasks.push(constructGruntTarget('concat', concatScriptsToGrantComponentTargetName, componentName));

        //2. Concat template only file
        tasks.push(constructGruntTarget('concat', concatTplsToGrantComponentTargetName, componentName));

        //make javascript files not vulnerable for uglify tasks
        tasks.push(constructGruntTarget('ngAnnotate', packageTargetName, componentName));

        //copy component's assets to dist, (styles are excluded)
        tasks.push(constructGruntTarget('copy', copyComponentToDiskTargetName, componentName));

        if (grunt.file.exists(styleFilePath)) {
            //minimize the css contents and copy the result to dist
            tasks.push(constructGruntTarget('cssmin', packageTargetName, componentName));
        }

        //uglify the javascript contents
        tasks.push(constructGruntTarget('uglify', packageTargetName, componentName));

        //give file unique name
        //constructGruntTarget('filerev', 'component', componentName)

        return tasks;
    }


    /**
     * Merge second list to first list
     * @param parentList
     * @param subList
     */
    function mergeList(parentList, subList) {
        if (!subList){
            return;
        }

        subList.forEach(function(item) {
            parentList.push(item);
        });
    }

    /**
     * Given path, if the path is file, return the file (array of 1).
     * If the path is directory, recursively get all the children files.
     * @param path
     * @returns {Array}
     */
    function getFileOrChildrenFiles(path) {
        var list = [];

        if (!fs.existsSync(path)) {
            return list;
        }

        var stat = fs.statSync(path);
        if (stat.isFile()){
            list.push(path);
        }

        if (stat.isDirectory()) {
            var files = getFilesRecursively(path);
            mergeList(list, files);
        }

        return list;
    }


    /**
     * Given a directory path, retrieve all the files recursively
     * @param path
     * @returns {Array}
     */
    function getFilesRecursively(path) {
        var list = [];

        if (!fs.existsSync(path)) {
            return list;
        }

        var files = fs.readdirSync(path);

        files.forEach(function(file) {
            var filePath = utility.constructPath(path, file);
            var files = getFileOrChildrenFiles(filePath);
            mergeList(list, files);
        });


        return list;
    }

    /**
     * Assets include styling files only, this function returns all the assets file path relative to the bower destination
     * @param bowerDest
     * @param grandComponentName
     * @returns {Array}
     */
    function getAssetsFilesForBower(bowerDest, grandComponentName) {
        var assets = [];

        var stylesPath = utility.constructPath(bowerDest, grandComponentName, 'styles');
        var styleFiles = getFilesRecursively(stylesPath);
        mergeList(assets, styleFiles);

        var strippedList = [];
        assets.forEach(function(file) {
            var strippedFilePath = file.replace(bowerDest + '/', '');
            strippedList.push(strippedFilePath);
        });

        return strippedList;
    }


    function getPackagedComponentsBowerJsonContent(options, dest, grandComponentName) {
        var version = options.version;
        var files = getAssetsFilesForBower(dest, grandComponentName);

        files.push(utility.constructPath(grandComponentName, grandComponentName + '.js'));

        return {
            name: grandComponentName,
            main: files,
            license: 'MIT',
            version: version
        };
    }

    grunt.registerMultiTask('packageComponents', 'package components', function () {
        var options = this.options({
            modulePrefixes: [],
            componentsDir: 'app/scripts/components',
            stylesDir: 'app/styles',
            fontsDir: 'app/fonts',
            imagesDir: 'app/images',
            templatesDir: 'app/views/templates',
            templatesCwd: null,
            grandComponentName: 'grand-component',
            version: '0.0.1'
        });

        var grandComponentName = utility.toHyphenFormat(options.grandComponentName);
        var dest = this.files[0].dest;

        registerTaskConfiguration(options, grandComponentName, dest);

        //register create bower json internal task
        grunt.registerTask('createBowerJson', function() {
            var done = this.async();

            var packageJsonFilePath = utility.constructPath(dest, 'bower.json');
            grunt.file.write(packageJsonFilePath, JSON.stringify(getPackagedComponentsBowerJsonContent(options, dest, grandComponentName), null, 2));

            done();
        });

        //return ordered list of components (the leaf components will starts from index 0)
        var components = inspector.getComponentMatrixList(options.componentsDir, options.modulePrefixes);

        //clean the component dist directory
        grunt.task.run([
            constructGruntTarget('clean', cleanComponentDistTaskName)
        ]);

        //package each components in proper order of their dependencies
        components.forEach(function (component) {
            var tasks = getComponentGruntTasks(component, options);
            grunt.task.run(tasks);
        });

        //for grand components
        grunt.task.run([
            //turn scss files to css and copy to dist
            constructGruntTarget('compass', compassToGrandComponentTargetName),
            constructGruntTarget('cssmin', cssminToGrandComponentDistTargetName),

            //make javascript files not vulnerable for uglify tasks
            constructGruntTarget('ngAnnotate', annotateGrandComponentTaskTargetName),

            //copy grand components' assets and script files
            constructGruntTarget('copy', copyGrandComponentsAssetsAndScriptsToDistTargetName),

            //uglify scripts
            constructGruntTarget('uglify', uglifyGrandComponentTaskTargetName),

            'createBowerJson'
        ]);


    })
};
