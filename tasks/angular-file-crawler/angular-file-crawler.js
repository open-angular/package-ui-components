module.exports = function(){

    function parseToList(phrase){
        if (!phrase) {
            return [];
        }

        if (_.isArray(phrase)) {
            return phrase;
        }

        return [phrase];
    }

    /**
     * Given content, a start phrase and end phrase, the method extract the phrase the encased start and end phrase.
     *
     * If no start phrase is given (null), it will assume to capture from beginning of content to end phrase.
     * If no end phrase is given (null), it will assume to capture from start phrase to the end of content.
     * If neither start phrase nor end phrase is given, the entire content is given back
     *
     * @param content
     * @param startPhrase
     * @param endPhrase
     * @returns {*}
     */
    function getSubstring(content, startPhrase, endPhrase) {
        var startIndex = 0;

        if (startPhrase) {
            var index = content.indexOf(startPhrase);

            if (index === -1) {
                return '';
            }

            startIndex = index + startPhrase.length;
            // info('Starting Index: ' + startIndex);
        }

        if (endPhrase) {
            var endIndex = content.indexOf(endPhrase, startIndex + 1);

            // info('Ending index: ' + endIndex);
            if (endIndex !== -1) {
                return content.substring(startIndex, endIndex);
            }
        }

        return content.substring(startIndex);
    }


    /**
     * Strip the single quote from phrase
     * @param phrase
     * @returns {*|{js}|XML|string|void}
     */
    function stripSingleQuote(phrase) {
        return phrase.trim().replace(/'/g, '');
    }


    /**
     *  Turn string of item separated by comma into array.
     * @param phrase
     */
    function string2Array(phrase) {
        var split = phrase.split(/\s*,\s*/);
        return split;
    }

    function flattenContent(content) {
        return content.replace(/\s*\r?\n\s*|\s*\r\s*/g, ' ');
    }

    function getMatchGroup(content, regex, groupIndex) {
        var match = regex.exec(content);
        if (!match){
            return '';
        }

        if (match.length < groupIndex) {
            return '';
        }

        var found = match[groupIndex + 1];
        return found;
    }

    function getDependentModuleNames(content){
        var modules = string2Array(stripSingleQuote(getMatchGroup(content, getModuleContentRegex, 0)));
        return modules;
    }

    /**
     * Given a list of modules, a list of supported module prefixes, this method filters the from the list of modules
     *  for those that starts with supported module prefixes
     * @param modules
     * @param supportedModulePrefixes
     * @returns {Array}
     */
    function filterForSupportedModules(modules, supportedModulePrefixes) {
        var supportedModules = [];

        modules.forEach(function (module) {
            var trimmedModule = module.trim();

            if (isModuleSupported(trimmedModule, supportedModulePrefixes)) {
                supportedModules.push(trimmedModule);
            }
        });

        return supportedModules;
    }

    /**
     * Given module and a list of supported module prefixes, this method check if the given module name starts with
     *  any of the supported prefixes.
     * @param module
     * @param supportedModulePrefixes
     * @returns {boolean}
     */
    function isModuleSupported(module, supportedModulePrefixes) {
        var found = false;
        supportedModulePrefixes.forEach(function (prefix) {

            if (module.indexOf(prefix) !== -1) {
                found = true;
            }
        });

        return found;
    }

    var getModuleContentRegex = /module\s*\(\s*['\.A-Za-z0-9]*\s*,\s*\[(.*)\]\)/;


    return {
        getSupportedDependentModules: function(flatContent, supportedModulePrefixes) {
            var dependentModules = getDependentModuleNames(flatContent);
            var supportedDependentModules = filterForSupportedModules(dependentModules, supportedModulePrefixes);
            return supportedDependentModules;
        },

        getDependentModules: function(flatContent) {
            var dependentModules = getDependentModuleNames(flatContent);
            return dependentModules;
        },

        getModuleName: function(flattenContent){
            var moduleName = stripSingleQuote(getMatchGroup(flattenContent, /module\s*\(\s*(['\.A-Za-z0-9]*)\s*,?.*\)\s*\./, 0));
            return moduleName;
        },

        getDirectiveName: function(flattenContent) {
            var directiveName = stripSingleQuote(getMatchGroup(flattenContent, /directive\s*\(\s*(['\.A-Za-z0-9]*)\s*,/, 0));
            return directiveName;
        },

        filterForSupportedModules: function(modules, supportedModulePrefixes) {
            var supportedModules = filterForSupportedModules(modules, supportedModulePrefixes);
            return supportedModules;
        },

        flattenContent: flattenContent
    };



};
