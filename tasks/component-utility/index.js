function ComponentUtility(){

    var _ = require('lodash');

    /**
     * Given 2 path segments, one is the root path and the second segment
     *  is the relative path to the root. This method combines the segments into new path.
     *
     * @param root
     * @param relativePath
     * @returns {*}
     */
    function joinPath(root, relativePath) {
        if (!root) {
            return relativePath;
        }

        if (!relativePath) {
            return root;
        }

        if (root.endsWith('/')) {
            return root + relativePath;
        }

        return root + "/" + relativePath;
    }

    /**
     * This method turns any phrase into lowerCamelFormat
     * @param phrase
     * @returns {*}
     */
    function toLowerCamelFormat(phrase) {
        return phrase.replace(/\-./g, function (value) {
            return value[1].toUpperCase();
        });
    }

    /**
     * This method turns any phrase into hyphen-format-phrase
     * @param phrase
     * @returns {*}
     */
    function toHyphenFormat(phrase) {
        return phrase.replace(/[A-Z]/g, function (value) {
            return "-" + value.toLowerCase();
        });
    }


    function joinSegments(firstSegment, secondSegment, joinPhrase) {
        if (!firstSegment) {
            return secondSegment;
        }

        if (!secondSegment) {
            return firstSegment;
        }

        if (firstSegment.endsWith(joinPhrase)) {
            return firstSegment + secondSegment;
        }

        return firstSegment + joinPhrase + secondSegment;
    }

    function constructPath() {
        var path = null;

        var pathSegments = _.values(arguments);
        pathSegments.forEach(function (arg) {
            path = joinPath(path, arg);
        });

        return path;
    }

    /**
     * Given path, this method get the last segment from the path
     * @param path
     * @returns {*}
     */
    function getFileOrDirectoryName(path) {
        var split = path.split('/');
        return split[split.length - 1];
    }

    function getDirectiveNamesFromModules(moduleNames) {
        var directives = [];
        moduleNames.forEach(function(module) {
            var split = module.split('.');
            var directiveName = split[split.length - 1];
            directives.push(directiveName);
        });

        return directives;
    }

    return {
        toLowerCamelFormat: toLowerCamelFormat,
        toHyphenFormat: toHyphenFormat,
        joinSegments: joinSegments,
        constructPath: constructPath,
        getFileOrDirectoryName: getFileOrDirectoryName,

        //given a list of module names, extract the last segment from its name, the last segment should be name the same as of directive.
        getDirectiveNamesFromModules: getDirectiveNamesFromModules
    };

}



module.exports = new ComponentUtility();

