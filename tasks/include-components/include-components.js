'use strict';

module.exports = function(grunt) {
    var _ = require('lodash');
    var crawler = require('../angular-file-crawler');
    var utility = require('../component-utility');

    var isWindows = process.platform === 'win32';

    /**
     * Correct the path symbol base on OS
     * Windows: c:\files\files.txt => will be changed to c:/files/files.txt
     * Linux: /path/file => remains unchanged.
     * @param path
     * @returns {*}
     */
    function updatePathBaseOnOS(path){
        if (isWindows){
            return path.replace(/\\/g, '/');
        } else {
            return path;
        }
    }

    function parseToList(phrase){
        if (!phrase) {
            return [];
        }

        if (_.isArray(phrase)) {
            return phrase;
        }

        return [phrase];
    }

    function info(msg) {
        grunt.log.writeln(msg);
    }


    function getParentPathInfo(filePath) {
        var split = filePath.split('/');
        split.pop();

        return {
            directoryName: split[split.length - 1],
            parentPath: split.join('/')
        };
    }

    function isFileUnderDirectory(filePath, directoryName) {
        if (!filePath || filePath === '') {
            return false;
        }

        var pathInfo = getParentPathInfo(filePath);
        if (pathInfo.directoryName === directoryName) {
            return true;
        }

        return isFileUnderDirectory(pathInfo.parentPath, directoryName);
    }

		/**
         * Check to see if the given file is under one of the given directory names.
         * This method is useful to check if the file is under the directive we need.
         * @param filePath
         * @param directoryNames
         * @returns {boolean}
         */
    function isFileUnderOneOfDirectories(filePath, directoryNames){
        for(var i = 0; i < directoryNames.length; i++){
            var directoryName = directoryNames[i];

            if (isFileUnderDirectory(filePath, directoryName)){
                return true;
            }
        }

        return false;
    }

    function toHyphenNames(directiveNames) {
        var names = [];

        directiveNames.forEach(function(camelname) {
            names.push(utility.toHyphenFormat(camelname));
        });

        return names;
    }

    grunt.registerMultiTask('includeComponents', 'include components listed from modules', function() {

        var componentFiles = this.filesSrc;

        var dest = this.files[0].dest;
        var scriptFiles = componentFiles.map(function(file) {
            return '<script src="' + file + '"></script>';
        });

        var destContent = grunt.file.read(dest);
        var updatedContent = destContent.replace(/<!--\s*include-components\s*-->/, scriptFiles.join('\n\t'));
        grunt.file.write(dest, updatedContent);

    });
};
